/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT)
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */
/*
 * Copyright 2007 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.xml.impl;

import com.jstype.web.client.xml.Node;
import com.jstype.web.client.xml.NodeList;

/**
 * This class implements the NodeList interface using the underlying
 * JavaScriptObject's implementation.
 */
class NodeListImpl extends DOMItem implements NodeList {

  protected NodeListImpl(Object o) {
    super(o);
  }

  public int getLength() {
    return XMLParserImpl.getLength(this.getJsObject());
  }

  /**
   * This method gets the index item.
   * 
   * @param index - the index to be retrieved
   * @return the item at this index
   * @see com.google.gwt.xml.client.NodeList#item(int)
   */
  public Node item(int index) {
    return NodeImpl.build(XMLParserImpl.item(this.getJsObject(), index));
  }

  @Override
  public String toString() {
    String b="";
    for (int i = 0; i < getLength(); i++) {
      b+=item(i).toString();
    }
    return b;
  }
}
