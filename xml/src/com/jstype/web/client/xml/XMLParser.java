/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT)
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.xml;

import com.jstype.web.client.xml.impl.XMLParserImpl;

import java.util.ArrayList;
import java.util.List;

public class XMLParser {

    private XMLParser() {
    }

    public static Document createDocument() {
        return XMLParserImpl.createDocument();
    }

    public static Document parse(String contents) {
        return XMLParserImpl.parse(contents);
    }
   
    public static void removeWhitespace(Node n) {
        removeWhitespaceInner(n, null);
    }

    public static boolean supportsCDATASection() {
        return false;
    }

    /*
     * The inner recursive method for removeWhitespace
     */
    private static void removeWhitespaceInner(Node n, Node parent) {
        // This n is removed from the parent if n is a whitespace node
        if (parent != null && n instanceof Text && (!(n instanceof CDATASection))) {
            Text t = (Text) n;
            if (t.getData().matches("[ \t\n]*")) {
                parent.removeChild(t);
            }
        }
        if (n.hasChildNodes()) {
            int length = n.getChildNodes().getLength();
            List<Node> toBeProcessed = new ArrayList<Node>();
            // We collect all the nodes to iterate as the child nodes will change
            // upon removal
            for (int i = 0; i < length; i++) {
                toBeProcessed.add(n.getChildNodes().item(i));
            }
            // This changes the child nodes, but the iterator of nodes never changes
            // meaning that this is safe
            for (Node childNode : toBeProcessed) {
                removeWhitespaceInner(childNode, n);
            }
        }
    }
}
